package cn.springspace.kotlinapp

import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters.fromObject
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

@Component
class RestBookHandler(val bookRepository: BookRepository) {
    fun saveBook(request: ServerRequest): Mono<ServerResponse> = request.bodyToMono(Book::class.java)
            .flatMap { bookRepository.save(it) }
            .flatMap { it ->
                ServerResponse.ok().body(fromObject(it))
            }
}